package com.example.dody.yourhelper.Api;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class Session {
    private SharedPreferences prefs;

    public Session(Context cntx) {
        // TODO Auto-generated constructor stub
        prefs = PreferenceManager.getDefaultSharedPreferences(cntx);
    }

    public void setusename(String usename) {
        prefs.edit().putString("usename", usename).commit();
    }

    public void setid_user(String id_user) {
        prefs.edit().putString("id_user", id_user).commit();
    }

    public String getusename() {
        String usename = prefs.getString("usename", "");
        return usename;
    }

    public String getid_user() {
        String id_user = prefs.getString("id_user", "");
        return id_user;
    }
}
