package com.example.dody.yourhelper.StaticVariable;

/**
 * Created by arimahardika on 01/03/2018.
 */

// THIS CLASS STORE ALL STATIC VARIABLE THAT USED IN THIS APP

public class StaticVariable {
    public static final String KEBAKARAN = "KEBAKARAN";
    public static final String BANJIR = "BANJIR";
    public static final String KECELAKAAN = "KECELAKAAN";
    public static final String MACET = "MACET";
    public static final String JALAN_BERLUBANG = "JALAN_BERLUBANG";
    public static final String LAMPU_MATI = "LAMPU_MATI";
    public static final String HOME = "HOME";

    public static final int REQUEST_LOCATION = 1;
}
