package com.example.dody.yourhelper.adapter;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import com.example.dody.yourhelper.Api.ApiClient;
import com.example.dody.yourhelper.Api.Session;
import com.example.dody.yourhelper.HomeActivity;
import com.example.dody.yourhelper.FeedsActivity;
import com.example.dody.yourhelper.Interface.PostinganInterface;
import com.example.dody.yourhelper.LoginActivity;
import com.example.dody.yourhelper.MapsActivity;
import com.example.dody.yourhelper.PostinganActivity;
import com.example.dody.yourhelper.R;
import com.example.dody.yourhelper.models.PostPutDelPostingan;
import com.example.dody.yourhelper.models.Postingan;
import com.squareup.picasso.Picasso;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.R.id.list;

public class PostinganAdapter extends RecyclerView.Adapter<PostinganAdapter.HolderData> {
    String alamat, kategori, number, id;
    int jumlah_call;
    PostinganInterface mPostinganInterface;
    Button btnTelpon;
    private List<Postingan> mListPostingan;
    private Context ctx;
    private Session session;

    public PostinganAdapter(Context ctx, List<Postingan> mListPostingan) {
        this.ctx = ctx;
        this.mListPostingan = mListPostingan;
    }

    @Override
    public HolderData onCreateViewHolder(ViewGroup parent, int viewType) {
        session = new Session(ctx);
        mPostinganInterface = ApiClient.getClient().create(PostinganInterface.class);
        View layout = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_feeds, parent, false);
        btnTelpon = (Button) layout.findViewById(R.id.btn_telpon);
        btnTelpon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (kategori.equals("Kebakaran")) {
                    number = "364617";
                } else {
                    number = "0341328553";
                }
                if (session.getid_user().equals("")) {
                    Toast.makeText(ctx, "Anda Harus Login Dulu !", Toast.LENGTH_LONG).show();
                    Intent in = new Intent(ctx, LoginActivity.class);
                    ctx.startActivity(in);
                } else {
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:" + number));
                    if (ActivityCompat.checkSelfPermission(ctx, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {

                    }
                    ctx.startActivity(intent);
                    jumlah_call = jumlah_call + 1;

                    RequestBody id_postingan = MultipartBody.create(MediaType.parse("multipart/form-data"), id);
                    RequestBody jml = MultipartBody.create(MediaType.parse("multipart/form-data"), Integer.toString(jumlah_call));

                    Call<PostPutDelPostingan> postKamarCall =
                            mPostinganInterface.putPostingan(jml, id_postingan);
                    postKamarCall.enqueue(new Callback<PostPutDelPostingan>() {
                        @Override
                        public void onResponse(Call<PostPutDelPostingan> call,
                                               Response<PostPutDelPostingan> response) {
                            Log.d("Insert", " Retrofit Insert: Berhasil Yess");
                        }

                        @Override
                        public void onFailure(Call<PostPutDelPostingan> call, Throwable t) {
                            Log.d("Insert", "Retrofit Insert: " + t.getMessage());
                        }
                    });
                }
            }
        });
        HolderData holder = new HolderData(layout);
        return holder;
    }

    @Override
    public void onBindViewHolder(HolderData holder, int position) {
        Postingan post = mListPostingan.get(position);
        id = post.getId_postingan();
        holder.tvNamaAkun.setText(post.getNama());
        holder.tvDeskripsi.setText(post.getDeskripsi());
        holder.tvAlamat.setText(post.getAlamat());
        holder.tvKategori.setText(post.getKategori());
        holder.tvStatus.setText(post.getStatus());
        if (post.getJumlah_call() > 3) {
            btnTelpon.setVisibility(View.GONE);

            RequestBody id_postingan = MultipartBody.create(MediaType.parse("multipart/form-data"), post.getId_postingan());
            RequestBody status = MultipartBody.create(MediaType.parse("multipart/form-data"), "Sudah Ditangani");

            Call<PostPutDelPostingan> postKamarCall =
                    mPostinganInterface.putStatusPostingan(status, id_postingan);
            postKamarCall.enqueue(new Callback<PostPutDelPostingan>() {
                @Override
                public void onResponse(Call<PostPutDelPostingan> call,
                                       Response<PostPutDelPostingan> response) {
                    Log.d("Insert", " Retrofit Insert: Berhasil Yess");
                }

                @Override
                public void onFailure(Call<PostPutDelPostingan> call, Throwable t) {
                    Log.d("Insert", "Retrofit Insert: " + t.getMessage());
                }
            });

        }
        kategori = post.getKategori();

        Log.d("Hasil kategori", "hasil : " + kategori);

        jumlah_call = post.getJumlah_call();
        alamat = post.getAlamat();
        Picasso.with(holder.itemView.getContext())
                .load("http://resttanggap.000webhostapp.com/" + post.getGambar())
                .resize(150, 150)
                .into(holder.img);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(view.getContext(), MapsActivity.class);
                i.putExtra("alamat", alamat);
                view.getContext().startActivity(i);
            }
        });
        holder.tvNoTelp.setText("Nomor Telepon : " + post.getNotelp());
        holder.tvTanggal.setText("diupload pada : " + post.getTanggal());
        holder.post = post;
    }

    @Override
    public int getItemCount() {
        return mListPostingan.size();
    }


    class HolderData extends RecyclerView.ViewHolder {
        TextView tvNamaAkun, tvDeskripsi, tvAlamat, tvKategori, tvNoTelp, tvStatus, tvTanggal;
        ImageView img;
        Postingan post;

        public HolderData(View v) {
            super(v);

            tvNamaAkun = (TextView) itemView.findViewById(R.id.nama_akun);
            tvDeskripsi = (TextView) itemView.findViewById(R.id.keterangan);
            tvAlamat = (TextView) itemView.findViewById(R.id.alamat);
            tvKategori = (TextView) itemView.findViewById(R.id.kategori);
            tvNoTelp = (TextView) itemView.findViewById(R.id.notelp);
            tvStatus = (TextView) itemView.findViewById(R.id.status);
            img = itemView.findViewById(R.id.foto_kejadian);
            tvTanggal = (TextView) itemView.findViewById(R.id.tanggal);
        }
    }
}
