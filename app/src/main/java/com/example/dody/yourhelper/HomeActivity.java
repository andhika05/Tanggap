package com.example.dody.yourhelper;

import android.Manifest;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.dody.yourhelper.Api.Session;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Button;
import android.widget.Toast;

import com.example.dody.yourhelper.Api.ApiClient;
import com.example.dody.yourhelper.Interface.PostinganInterface;
import com.example.dody.yourhelper.StaticVariable.StaticVariable;
import com.example.dody.yourhelper.adapter.PostinganAdapter;
import com.example.dody.yourhelper.models.GetPostingan;
import com.example.dody.yourhelper.models.Postingan;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeActivity extends AppCompatActivity {

    //<editor-fold desc="Initialize Variable">
    Double startLat, startLng;
    ProgressDialog pd;
    LocationManager locationManager;
    PostinganInterface api;
    Call<GetPostingan> getdata;
    private RecyclerView mRecycler;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mManager;
    private List<Postingan> mItems = new ArrayList<>();
    private Session session;
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mToggle;
    //</editor-fold>

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        ButterKnife.bind(this);

        session = new Session(getApplicationContext());

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        getLocation();

        initializeFirstData();
    }

    private void initializeFirstData() {
        pd = new ProgressDialog(this);
        mRecycler = (RecyclerView) findViewById(R.id.rv_postingan);
        mManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        mRecycler.setLayoutManager(mManager);

        pd.setMessage("Loading...");
        pd.setCancelable(false);
        pd.show();
        PostinganInterface api = ApiClient.getClient().create(PostinganInterface.class);
        Call<GetPostingan> getdata = api.getPostingan();
        getdata.enqueue(new Callback<GetPostingan>() {
            @Override
            public void onResponse(Call<GetPostingan> call, Response<GetPostingan> response) {
                pd.hide();
                mItems = response.body().getListDataPostingan();
                mAdapter = new PostinganAdapter(HomeActivity.this, mItems);
                mRecycler.setAdapter(mAdapter);
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<GetPostingan> call, Throwable t) {
                Log.d("RETRO", "RESPONSE : respon gagal");
            }
        });
    }

    @OnClick({R.id.tambah_post, R.id.logout, R.id.kebakaran, R.id.banjir,
            R.id.kecelakaan, R.id.macet, R.id.jalan, R.id.lampu, R.id.home})
    public void onClickCategory(Button button) {
        switch (button.getId()) {
            case R.id.tambah_post:
                //<editor-fold desc="Tambah Post">
                if (session.getid_user().equals("")) {
                    Toast.makeText(HomeActivity.this, "Anda Harus Login Dulu !", Toast.LENGTH_LONG).show();
                    Intent in = new Intent(HomeActivity.this, LoginActivity.class);
                    startActivity(in);
                } else {
                    startActivity(new Intent(HomeActivity.this, PostinganActivity.class));
                }
                //</editor-fold>
                break;
            case R.id.logout:
                //<editor-fold desc="Logout">
                session.setid_user("");
                session.setusename("");
                Intent in = new Intent(HomeActivity.this, LoginActivity.class);
                startActivity(in);
                break;
            //</editor-fold>
            case R.id.kebakaran:
                getDataFromEachCategory(StaticVariable.KEBAKARAN);
                break;
            case R.id.banjir:
                getDataFromEachCategory(StaticVariable.BANJIR);
                break;
            case R.id.kecelakaan:
                getDataFromEachCategory(StaticVariable.KECELAKAAN);
                break;
            case R.id.macet:
                getDataFromEachCategory(StaticVariable.MACET);
                break;
            case R.id.jalan:
                getDataFromEachCategory(StaticVariable.JALAN_BERLUBANG);
                break;
            case R.id.lampu:
                getDataFromEachCategory(StaticVariable.LAMPU_MATI);
                break;
            case R.id.home:
                getDataFromEachCategory(StaticVariable.HOME);
                break;
        }
    }

    public void getDataFromEachCategory(String category) {
        pd.setMessage("Loading...");
        pd.setCancelable(false);
        pd.show();

        api = ApiClient.getClient().create(PostinganInterface.class);

        switch (category) {
            case StaticVariable.HOME:
                getdata = api.getPostingan();
                break;
            case StaticVariable.KEBAKARAN:
                getdata = api.getPostinganKebakaran();
                break;
            case StaticVariable.BANJIR:
                getdata = api.getPostinganBanjir();
                break;
            case StaticVariable.KECELAKAAN:
                getdata = api.getPostinganKecelakaan();
                break;
            case StaticVariable.JALAN_BERLUBANG:
                getdata = api.getPostinganJalanBerlubang();
                break;
            case StaticVariable.LAMPU_MATI:
                getdata = api.getPostinganLampuJalanMati();
                break;
            case StaticVariable.MACET:
                getdata = api.getPostinganKemacetan();
                break;
        }

        getdata.enqueue(new Callback<GetPostingan>() {
            @Override
            public void onResponse(Call<GetPostingan> call, Response<GetPostingan> response) {
                pd.hide();
                mItems = response.body().getListDataPostingan();
                mAdapter = new PostinganAdapter(HomeActivity.this, mItems);
                mRecycler.setAdapter(mAdapter);
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<GetPostingan> call, Throwable t) {
                Log.d("RETRO", "RESPONSE : respon gagal");
            }
        });
    }

    void getLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    StaticVariable.REQUEST_LOCATION);
        } else {
            Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

            if (location != null) {
                double latti = location.getLatitude();
                double longi = location.getLongitude();

                startLat = latti;
                startLng = longi;
            }

        }
    }
}
