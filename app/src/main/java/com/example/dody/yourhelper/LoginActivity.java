package com.example.dody.yourhelper;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import retrofit2.Callback;
import retrofit2.Response;

import com.example.dody.yourhelper.Api.ApiClient;
import com.example.dody.yourhelper.Api.Session;
import com.example.dody.yourhelper.Interface.UserInterface;
import com.example.dody.yourhelper.models.PostPutDelUser;
import com.example.dody.yourhelper.models.User;

import java.util.List;

public class LoginActivity extends AppCompatActivity {
    EditText edtUser, edtPass;
    Button btLogin, btRegister;
    UserInterface mUserInterface;
    ProgressDialog pd;
    private Session session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        session = new Session(getApplicationContext());

        edtUser = (EditText) findViewById(R.id.user);
        edtPass = (EditText) findViewById(R.id.pass);
        btLogin = (Button) findViewById(R.id.btn_login);
        btRegister = (Button) findViewById(R.id.btn_link_signup);

        pd = new ProgressDialog(this) {
            public void onBackPressed() {
                finish();
            }
        };
        mUserInterface = ApiClient.getClient().create(UserInterface.class);

        btLogin.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                //<editor-fold desc="public void onClick(View view)">
                pd.setMessage("Loading...");
                pd.setCancelable(false);
                pd.show();
                if (edtUser.getText().toString().equals("")) {
                    pd.dismiss();
                    Toast.makeText(LoginActivity.this, "Username Wajib Di Isi!", Toast.LENGTH_LONG).show();
                } else if (edtPass.getText().toString().equals("")) {
                    pd.dismiss();
                    Toast.makeText(LoginActivity.this, "Password Wajib Di Isi!", Toast.LENGTH_LONG).show();
                } else {
                    mUserInterface = ApiClient.getClient().create(UserInterface.class);
                    retrofit2.Call<List<User>> loginCall = mUserInterface.cekLogin(edtUser.getText().toString(), edtPass.getText().toString());
                    loginCall.enqueue(new Callback<List<User>>() {
                        @Override
                        public void onResponse(retrofit2.Call<List<User>> call, Response<List<User>> response) {
                            if (response.isSuccessful()) {
                                List<User> login = response.body();
                                if (login.get(0).getNoTelp().equals(edtUser.getText().toString())) {
                                    //Pindah Activity
                                    session.setusename(login.get(0).getNama());
                                    session.setid_user(login.get(0).getIdUser());
                                    Intent i = new Intent(LoginActivity.this, HomeActivity.class);
                                    startActivity(i);
                                } else {
                                    pd.dismiss();
                                    Toast.makeText(LoginActivity.this, "Email Atau Password Salah", Toast.LENGTH_LONG).show();
                                }
                            } else {
                                pd.dismiss();
                                Toast.makeText(LoginActivity.this, "Error Coba Lagi", Toast.LENGTH_LONG).show();
                            }
                        }

                        @Override
                        public void onFailure(retrofit2.Call<List<User>> call, Throwable t) {
                            pd.dismiss();
                            Toast.makeText(LoginActivity.this, t.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    });
                }
                //</editor-fold>
            }
        });

        btRegister.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent in = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(in);
            }
        });
    }

    protected void onDestroy() {
        if (pd != null && pd.isShowing()) {
            super.onDestroy();
        }
    }
}
