package com.example.dody.yourhelper.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetPostingan {
    @SerializedName("status")
    String status;
    @SerializedName("result")
    List<Postingan> result;
    @SerializedName("message")
    String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    public List<Postingan> getListDataPostingan() {
        return result;
    }

    public void setListDataPostingan(List<Postingan> listDataPostingan) {
        this.result = listDataPostingan;
    }
}
