package com.example.dody.yourhelper.Interface;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

import com.example.dody.yourhelper.models.PostPutDelUser;
import com.example.dody.yourhelper.models.User;

import java.util.List;

public interface UserInterface {
    @FormUrlEncoded
    @POST("user")
    Call<PostPutDelUser> postUser(@Field("nama") String nama,
                                  @Field("no_telp") String no_telp,
                                  @Field("password") String password);

    @GET("user/login")
    Call<List<User>> cekLogin(@Query("username") String username,
                              @Query("password") String password);
}
