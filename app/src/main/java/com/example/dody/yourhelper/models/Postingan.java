package com.example.dody.yourhelper.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Postingan {

    //region SerializedName
    @SerializedName("id_postingan")
    @Expose
    private String id_postingan;
    @SerializedName("id_user")
    @Expose
    private String id_user;
    @SerializedName("kategori")
    @Expose
    private String kategori;
    @SerializedName("latitude")
    @Expose
    private double latitude;
    @SerializedName("longitude")
    @Expose
    private double longitude;
    @SerializedName("alamat")
    @Expose
    private String alamat;
    @SerializedName("deskripsi")
    @Expose
    private String deskripsi;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("gambar")
    @Expose
    private String gambar;
    @SerializedName("nama")
    @Expose
    private String nama;
    @SerializedName("no_telp")
    @Expose
    private String notelp;
    @SerializedName("tanggal")
    @Expose
    private String tanggal;
    @SerializedName("jumlah_call")
    @Expose
    private int jumlah_call;
    //endregion

    //region Constructor
    public Postingan(String id_postingan, String id_user, String kategori, double latitude, double longitude, String alamat, String deskripsi, String status, String gambar, String nama, String notelp, String tanggal, int jumlah_call) {
        this.id_postingan = id_postingan;
        this.id_user = id_user;
        this.kategori = kategori;
        this.latitude = latitude;
        this.longitude = longitude;
        this.alamat = alamat;
        this.deskripsi = deskripsi;
        this.status = status;
        this.gambar = gambar;
        this.nama = nama;
        this.notelp = notelp;
        this.tanggal = tanggal;
        this.jumlah_call = jumlah_call;
    }
    //endregion

    public String getId_postingan() {
        return id_postingan;
    }

    //region Setter
    public void setId_postingan(String id_postingan) {
        this.id_postingan = id_postingan;
    }

    //region Getter
    public String getId_user() {
        return id_user;
    }

    public void setId_user(String id_user) {
        this.id_user = id_user;
    }

    public String getKategori() {
        return kategori;
    }

    public void setKategori(String kategori) {
        this.kategori = kategori;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }
    //endregion

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getGambar() {
        return gambar;
    }

    public void setGambar(String gambar) {
        this.gambar = gambar;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getNotelp() {
        return notelp;
    }

    public void setNotelp(String notelp) {
        this.notelp = notelp;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public int getJumlah_call() {
        return jumlah_call;
    }

    public void setJumlah_call(int jumlah_call) {
        this.jumlah_call = jumlah_call;
    }
    //endregion
}
