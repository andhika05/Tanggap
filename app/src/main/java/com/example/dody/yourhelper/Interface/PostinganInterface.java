package com.example.dody.yourhelper.Interface;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Query;

import com.example.dody.yourhelper.models.GetPostingan;
import com.example.dody.yourhelper.models.PostPutDelPostingan;
import com.example.dody.yourhelper.models.Postingan;

public interface PostinganInterface {

    @Multipart
    @POST("postingan")
    Call<PostPutDelPostingan> postPostingan(@Part("id_postingan") RequestBody id_postingan,
                                            @Part("id_user") RequestBody id_user,
                                            @Part("kategori") RequestBody kategori,
                                            @Part("latitude") RequestBody latitude,
                                            @Part("longitude") RequestBody longitude,
                                            @Part("alamat") RequestBody alamat,
                                            @Part("deskripsi") RequestBody deskripsi,
                                            @Part("status") RequestBody status,
                                            @Part MultipartBody.Part file,
                                            @Part("tanggal") RequestBody tanggal,
                                            @Part("jumlah_call") RequestBody jumlah_call);

    @Multipart
    @POST("postingan/updatejumlahcall")
    Call<PostPutDelPostingan> putPostingan(@Part("jumlah_call") RequestBody jml,
                                           @Part("id_postingan") RequestBody id);

    @Multipart
    @POST("postingan/updatestatus")
    Call<PostPutDelPostingan> putStatusPostingan(@Part("status") RequestBody status,
                                                 @Part("id_postingan") RequestBody id);

    @GET("postingan")
    Call<GetPostingan> getPostingan();

    @GET("postingan/kebakaran")
    Call<GetPostingan> getPostinganKebakaran();

    @GET("postingan/banjir")
    Call<GetPostingan> getPostinganBanjir();

    @GET("postingan/kecelakaan")
    Call<GetPostingan> getPostinganKecelakaan();

    @GET("postingan/kemacetan")
    Call<GetPostingan> getPostinganKemacetan();

    @GET("postingan/jalanberlubang")
    Call<GetPostingan> getPostinganJalanBerlubang();

    @GET("postingan/lampujalanmati")
    Call<GetPostingan> getPostinganLampuJalanMati();
}
