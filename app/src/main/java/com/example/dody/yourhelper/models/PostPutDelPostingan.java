package com.example.dody.yourhelper.models;

import com.google.gson.annotations.SerializedName;

public class PostPutDelPostingan {
    @SerializedName("status")
    String status;
    @SerializedName("result")
    Postingan mPostingan;
    @SerializedName("kode")
    String kode;
    @SerializedName("message")
    String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Postingan getPostingan() {
        return mPostingan;
    }

    public void setPostingan(Postingan postingan) {
        mPostingan = postingan;
    }
}
