package com.example.dody.yourhelper;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import com.example.dody.yourhelper.models.PostPutDelUser;
import com.example.dody.yourhelper.Api.ApiClient;
import com.example.dody.yourhelper.Interface.UserInterface;
import com.example.dody.yourhelper.models.User;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegisterActivity extends AppCompatActivity {

    EditText edtNama, edtEmail, edtNoTelp, edtUsername, edtPassword;
    Button btInsert, btBack;
    UserInterface mUserInterface;
    ProgressDialog pd;
    boolean bs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        edtNama = findViewById(R.id.editNamaRegister);
        edtNoTelp = findViewById(R.id.editNomorTelepon);
        edtPassword = findViewById(R.id.editPassRegister);
        btInsert = findViewById(R.id.btnRegister);
        btBack = findViewById(R.id.btnKembali);

        pd = new ProgressDialog(this);
        mUserInterface = ApiClient.getClient().create(UserInterface.class);

        btInsert.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                //<editor-fold desc="Onclick">
                Pattern p = Pattern.compile("^[0-9]+$");
                Matcher m = p.matcher(edtNoTelp.getText().toString());
                bs = m.matches();
                pd.setMessage("Loading...");
                pd.setCancelable(false);
                pd.show();
                if (edtNama.getText().toString().equals("")) {
                    pd.dismiss();
                    Toast.makeText(RegisterActivity.this, "Nama Lengkap Wajib Di Isi!", Toast.LENGTH_LONG).show();
                } else if (edtNoTelp.getText().toString().equals("")) {
                    pd.dismiss();
                    Toast.makeText(RegisterActivity.this, "Nomor Telepon Wajib Di Isi!", Toast.LENGTH_LONG).show();
                } else if (bs == false) {
                    pd.dismiss();
                    Toast.makeText(RegisterActivity.this, "Nomor Telepon Harus Angka!", Toast.LENGTH_LONG).show();
                } else if (edtPassword.getText().toString().equals("")) {
                    pd.dismiss();
                    Toast.makeText(RegisterActivity.this, "Password Wajib Di Isi!", Toast.LENGTH_LONG).show();
                } else {
                    Call<PostPutDelUser> postMahasiswaCall = mUserInterface.postUser(edtNama.getText().toString(), edtNoTelp.getText().toString(), edtPassword.getText().toString());
                    postMahasiswaCall.enqueue(new Callback<PostPutDelUser>() {
                        @Override
                        public void onResponse(Call<PostPutDelUser> call, Response<PostPutDelUser> response) {
                            Toast.makeText(RegisterActivity.this, "Berhasil Register", Toast.LENGTH_LONG).show();
                            Intent in = new Intent(RegisterActivity.this, LoginActivity.class);
                            startActivity(in);
                        }

                        @Override
                        public void onFailure(Call<PostPutDelUser> call, Throwable t) {
                            pd.dismiss();
                            Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG).show();
                        }
                    });
                }
                //</editor-fold>
            }
        });

        btBack.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent in = new Intent(RegisterActivity.this, LoginActivity.class);
                startActivity(in);
            }
        });
    }
}
