package com.example.dody.yourhelper;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.CalendarContract;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;
import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;

import com.example.dody.yourhelper.Api.ApiClient;
import com.example.dody.yourhelper.Api.Session;
import com.example.dody.yourhelper.Interface.PostinganInterface;
import com.example.dody.yourhelper.Interface.UserInterface;
import com.example.dody.yourhelper.models.PostPutDelPostingan;
import com.example.dody.yourhelper.models.PostPutDelUser;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PostinganActivity extends AppCompatActivity {

    //<editor-fold desc="Initialize Variable">
    static final int REQUEST_LOCATION = 1;
    static final int READ_EXTERNAL = 2;
    static final int WRITE_EXTERNAL = 3;
    EditText edtDeskripsi;
    Spinner spKategori;
    ImageButton foto;
    Button btnBatal, btnKirim;
    String user, lat, lng, address;
    PostinganInterface mPostinganInterface;
    ProgressDialog pd;
    LocationManager locationManager;
    String imagePath = "";
    File mFileUri;
    Bitmap originalImage;
    int width;
    int height;
    int newWidth = 200;
    int newHeight = 200;
    Matrix matrix;
    Bitmap resizedBitmap;
    float scaleWidth;
    float scaleHeight;
    ByteArrayOutputStream outputStream;
    private Session session;
    //</editor-fold>

    private static File getMediaFileName() {
        // Lokasi External sdcard
        File mediaStorageDir = new
                File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                "CameraDemo");
        // Buat directori tidak direktori tidak eksis
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d("CameraDemo", "Gagal membuat directory" + "CameraDemo");
                return null;
            }
        }
        File mediaFile = null;
        // Membuat nama file
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + "IMG_" + timeStamp
                + ".jpg");
        return mediaFile;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_postingan);
        session = new Session(getApplicationContext());

        pd = new ProgressDialog(this) {
            public void onBackPressed() {
                finish();
            }
        };
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        getLocation();
        edtDeskripsi = (EditText) findViewById(R.id.desk);
        spKategori = (Spinner) findViewById(R.id.kategori);
        btnBatal = (Button) findViewById(R.id.batal);
        btnKirim = (Button) findViewById(R.id.kirim);
        foto = (ImageButton) findViewById(R.id.foto);
        user = session.getid_user();
        address = getCompleteAddressString(Double.valueOf(lat).doubleValue(), Double.valueOf(lng).doubleValue());

        mPostinganInterface = ApiClient.getClient().create(PostinganInterface.class);

        btnKirim.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                if (edtDeskripsi.getText().toString().equals("")) {
                    Toast.makeText(PostinganActivity.this, "Isi Deskripsi!", Toast.LENGTH_LONG).show();
                } else {
                    pd.setMessage("Loading...");
                    pd.setCancelable(false);
                    pd.show();
                    MultipartBody.Part body = null;

                    if (!imagePath.isEmpty()) {
                        File file = new File(imagePath);


                        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);

                        body = MultipartBody.Part.createFormData("gambar", file.getName(), requestFile);
                    }

                    RequestBody id_postingan = MultipartBody.create(MediaType.parse("multipart/form-data"), "");
                    RequestBody id_user = MultipartBody.create(MediaType.parse("multipart/form-data"), user);
                    RequestBody kategori = MultipartBody.create(MediaType.parse("multipart/form-data"), spKategori.getSelectedItem().toString());
                    RequestBody latitude = MultipartBody.create(MediaType.parse("multipart/form-data"), lat);
                    RequestBody longitude = MultipartBody.create(MediaType.parse("multipart/form-data"), lng);
                    RequestBody alamat = MultipartBody.create(MediaType.parse("multipart/form-data"), address);
                    RequestBody deskripsi = MultipartBody.create(MediaType.parse("multipart/form-data"), edtDeskripsi.getText().toString());
                    RequestBody status = MultipartBody.create(MediaType.parse("multipart/form-data"), "Belum Ditangani");
                    RequestBody tanggal = MultipartBody.create(MediaType.parse("multipart/form-data"), "");
                    RequestBody jumlah_call = MultipartBody.create(MediaType.parse("multipart/form-data"), "");

                    Call<PostPutDelPostingan> postKamarCall =
                            mPostinganInterface.postPostingan(id_postingan, id_user, kategori, latitude, longitude, alamat, deskripsi, status, body, tanggal, jumlah_call);
                    postKamarCall.enqueue(new Callback<PostPutDelPostingan>() {
                        @Override
                        public void onResponse(Call<PostPutDelPostingan> call,
                                               Response<PostPutDelPostingan> response) {
                            Log.d("Insert", " Retrofit Insert: Berhasil Yess");
                            Intent in = new Intent(PostinganActivity.this, HomeActivity.class);
                            startActivity(in);
                        }

                        @Override
                        public void onFailure(Call<PostPutDelPostingan> call, Throwable t) {
                            Log.d("Insert", "Retrofit Insert: " + t.getMessage());
                        }
                    });
                }
            }
        });

        btnBatal.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent in = new Intent(PostinganActivity.this, HomeActivity.class);
                startActivity(in);
            }
        });

        foto.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                selectImage();
            }
        });
    }

    private void selectImage() {
        final CharSequence[] items = {"Camera", "Gallery", "Batal"};

        AlertDialog.Builder builder = new AlertDialog.Builder(PostinganActivity.this);
        builder.setTitle("Upload Foto");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (items[i].equals("Camera")) {

                    captureImage();

                } else if (items[i].equals("Gallery")) {

                    Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(intent.createChooser(intent, "Pilih Gambar"), 2);

                } else if (items[i].equals("Batal")) {

                    dialogInterface.dismiss();
                }
            }
        }).show();
    }

    private void captureImage() {
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            mFileUri = getMediaFileName();
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, CalendarContract.CalendarCache.URI.fromFile(mFileUri));
            startActivityForResult(takePictureIntent, 1);
        }
    }

    private String getCompleteAddressString(double LATITUDE, double LONGITUDE) {
        String strAdd = "";
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder("");

                for (int i = 0; i <= returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                strAdd = strReturnedAddress.toString();
                //Toast.makeText(PostinganActivity.this, "My Current loction address = "+strReturnedAddress.toString(), Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(PostinganActivity.this, "My Current loction address = No Address returned!", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(PostinganActivity.this, "My Current loction address = Canont get Address!", Toast.LENGTH_LONG).show();
        }
        return strAdd;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == 1) {
                originalImage = BitmapFactory.decodeFile(mFileUri.getPath());

                width = originalImage.getWidth();
                height = originalImage.getHeight();

                matrix = new Matrix();

                scaleWidth = ((float) newWidth) / width;

                scaleHeight = ((float) newHeight) / height;

                matrix.postScale(scaleWidth, scaleHeight);

                matrix.postRotate(0);

                resizedBitmap = Bitmap.createBitmap(originalImage, 0, 0, width, height, matrix, true);
//            outputStream = new ByteArrayOutputStream();

                FileOutputStream out = null;
                String filename = mFileUri.getPath();

                try {
                    out = new FileOutputStream(filename);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                resizedBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);

                Picasso.with(getApplicationContext()).load(new File(mFileUri.getPath())).into(foto);
                Log.d("cek lokasi : ", "" + mFileUri.getPath());

                imagePath = mFileUri.getPath();

            } else if (requestCode == 2) {
                if (data == null) {
                    Toast.makeText(getApplicationContext(), "Gambar Gagal Di load",
                            Toast.LENGTH_LONG).show();
                    return;
                }

                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};
                Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);

                if (cursor != null) {
                    cursor.moveToFirst();
                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    imagePath = cursor.getString(columnIndex);

                    Picasso.with(getApplicationContext()).load(new File(imagePath)).fit().into(foto);
                    cursor.close();
                } else {
                    Toast.makeText(getApplicationContext(), "Gambar Gagal Di load",
                            Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    void getLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
        } else {
            Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

            if (location != null) {
                double latti = location.getLatitude();
                double longi = location.getLongitude();

                lat = Double.toString(latti);
                lng = Double.toString(longi);
            }

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case REQUEST_LOCATION:
                getLocation();
                break;
            case READ_EXTERNAL:
                break;
            case WRITE_EXTERNAL:
                break;
        }
    }

    protected void onDestroy() {
        if (pd != null && pd.isShowing()) {
            super.onDestroy();
        }
    }
}
